# Wrestling App

Wrestle Mania (working title) is a an app to manage and track high school wrestling teams.

## Primary Use Cases & Objects (wip)
### Schools
### Seasons
### Teams
### Wrestlers
#### Get wrestler_season
Given a wrestler and a season returns an object.
```
http://wrestle_mania/api/vi/wrestler_season
```
#### Get wrestler_seasons
```
http://wrestle_mania/api/vi/wrestler_seasons
```
#### Get wrestler_season_matches
```
http://wrestle_mania/api/vi/wrestler_season_matches
```
### Coaches
#### Get coach_season
```
http://wrestle_mania/api/vi/coach_season/
```
### Meets
#### Get meets_season
### Matches

## Schema (evolving)
### These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
```
  create_table "coach_seasons", force: :cascade do |t|
    t.bigint "coach_id"
    t.bigint "team_id"
    t.bigint "season_id"
    t.integer "wins"
    t.integer "losses"
    t.integer "draws"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["coach_id"], name: "index_coach_seasons_on_coach_id"
    t.index ["season_id"], name: "index_coach_seasons_on_season_id"
    t.index ["team_id"], name: "index_coach_seasons_on_team_id"
  end

  create_table "coaches", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "match_scores", force: :cascade do |t|
    t.integer "score_id"
    t.integer "wrestler_id"
    t.integer "match_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "round"
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "meet_id"
    t.integer "green_score"
    t.integer "red_score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "red_wrestler_id"
    t.bigint "green_wrestler_id"
    t.index ["green_wrestler_id"], name: "index_matches_on_green_wrestler_id"
    t.index ["meet_id"], name: "index_matches_on_meet_id"
    t.index ["red_wrestler_id"], name: "index_matches_on_red_wrestler_id"
  end

  create_table "meets", force: :cascade do |t|
    t.date "date"
    t.integer "red_score"
    t.integer "green_score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "red_team_id"
    t.bigint "green_team_id"
    t.bigint "season_id"
    t.index ["green_team_id"], name: "index_meets_on_green_team_id"
    t.index ["red_team_id"], name: "index_meets_on_red_team_id"
    t.index ["season_id"], name: "index_meets_on_season_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "addr1"
    t.string "addr2"
    t.string "city"
    t.string "state_provence"
    t.string "postal_code"
    t.string "mascot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "scores", force: :cascade do |t|
    t.string "score_type"
    t.integer "score_points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seasons", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "season"
  end

  create_table "seasons_teams", id: false, force: :cascade do |t|
    t.integer "season_id"
    t.integer "team_id"
  end

  create_table "team_seasons", force: :cascade do |t|
    t.bigint "team_id"
    t.bigint "season_id"
    t.integer "wins"
    t.integer "losses"
    t.integer "draws"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["season_id"], name: "index_team_seasons_on_season_id"
    t.index ["team_id"], name: "index_team_seasons_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_id"
    t.index ["school_id"], name: "index_teams_on_school_id"
  end

  create_table "wrestler_seasons", force: :cascade do |t|
    t.bigint "wrestler_id"
    t.bigint "team_id"
    t.bigint "season_id"
    t.integer "win_decision"
    t.integer "win_major_decision"
    t.integer "win_forfeit"
    t.integer "win_pin"
    t.integer "loss_decision"
    t.integer "loss_major_decision"
    t.integer "loss_forfeit"
    t.integer "loss_pin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["season_id"], name: "index_wrestler_seasons_on_season_id"
    t.index ["team_id"], name: "index_wrestler_seasons_on_team_id"
    t.index ["wrestler_id"], name: "index_wrestler_seasons_on_wrestler_id"
  end

  create_table "wrestlers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "weight_class"
    t.integer "academic_class"
  end

  add_foreign_key "coach_seasons", "coaches"
  add_foreign_key "coach_seasons", "seasons"
  add_foreign_key "coach_seasons", "teams"
  add_foreign_key "matches", "meets"
  add_foreign_key "meets", "seasons"
  add_foreign_key "team_seasons", "seasons"
  add_foreign_key "team_seasons", "teams"
  add_foreign_key "teams", "schools"
  add_foreign_key "wrestler_seasons", "seasons"
  add_foreign_key "wrestler_seasons", "teams"
  add_foreign_key "wrestler_seasons", "wrestlers"
end
