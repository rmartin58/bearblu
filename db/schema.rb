# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180417191810) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "coaches", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.string "email"
    t.string "phone_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "team_id"
    t.index ["team_id"], name: "index_coaches_on_team_id"
  end

  create_table "match_scores", force: :cascade do |t|
    t.integer "wrestler_id"
    t.integer "match_id"
    t.integer "round", default: 1
    t.string "description"
    t.integer "points"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matches", force: :cascade do |t|
    t.bigint "meet_id"
    t.integer "green_score", default: 0, null: false
    t.integer "red_score", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "red_wrestler_id"
    t.bigint "green_wrestler_id"
    t.boolean "match_completed", default: false
    t.integer "round", default: 1
    t.index ["green_wrestler_id"], name: "index_matches_on_green_wrestler_id"
    t.index ["meet_id"], name: "index_matches_on_meet_id"
    t.index ["red_wrestler_id"], name: "index_matches_on_red_wrestler_id"
  end

  create_table "meets", force: :cascade do |t|
    t.date "date"
    t.integer "red_score", default: 0
    t.integer "green_score", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "red_team_id"
    t.bigint "green_team_id"
    t.index ["green_team_id"], name: "index_meets_on_green_team_id"
    t.index ["red_team_id"], name: "index_meets_on_red_team_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "addr1"
    t.string "addr2"
    t.string "city"
    t.string "state_provence"
    t.string "postal_code"
    t.string "mascot"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "season_wrestlers", force: :cascade do |t|
    t.string "season"
    t.integer "wins_by_pin", default: 0
    t.integer "wins_by_forfeit", default: 0
    t.integer "wins_by_decision", default: 0
    t.integer "wins_by_major_decision", default: 0
    t.integer "losses_by_pin", default: 0
    t.integer "losses_by_forfeit", default: 0
    t.integer "losses_by_decision", default: 0
    t.integer "losses_by_major_decision", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "take_downs", default: 0
    t.integer "escapes", default: 0
    t.integer "reversals", default: 0
    t.integer "near_fall_2", default: 0
    t.integer "near_fall_3", default: 0
    t.integer "near_fall_4", default: 0
    t.integer "penalties", default: 0
    t.integer "cautions", default: 0
    t.integer "disqualifications", default: 0
    t.bigint "team_id"
    t.integer "weight_class"
    t.integer "academic_class"
    t.bigint "wrestler_id"
    t.index ["team_id"], name: "index_season_wrestlers_on_team_id"
    t.index ["wrestler_id"], name: "index_season_wrestlers_on_wrestler_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "school_id"
    t.string "season"
    t.integer "wins"
    t.integer "losses"
    t.integer "draws"
    t.index ["school_id"], name: "index_teams_on_school_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "wrestlers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "coaches", "teams"
  add_foreign_key "match_scores", "matches"
  add_foreign_key "match_scores", "wrestlers"
  add_foreign_key "matches", "meets"
  add_foreign_key "season_wrestlers", "teams"
  add_foreign_key "season_wrestlers", "wrestlers"
  add_foreign_key "teams", "schools"
end
