require 'faker'

weight_class = [106, 113, 120, 126, 132, 138, 145, 152, 160, 170, 182, 195, 220, 285]
academic_class = [9, 10, 11, 12]
scores = { take_down: 2, escape: 1, reversal: 2, near_fall_2: 2, near_fall_3: 3, penalty: 1 }
SEASON_2018 = '2018-2019'
SEASON_2017 = '2017-2018'
SEASON_2016 = '2016-2017'
SEASON_2015 = '2015-2016'
puts "created 4 seasons"
# create school
puts "Create ISSY"
school = School.create(
  name: 'Issaquah HS',
  addr1: '700 2nd Avenue SE',
  city: 'Issaquah',
  state_provence: 'WA',
  postal_code: '98027',
  mascot: 'Eagles'
)

# create a team for the school for each season
puts 'Creating a team'
boys_varsity = Team.create(school: school, name: 'Boys Varsity', season: SEASON_2018)
boys_jv = Team.create(school: school, name: 'Boys JV', season: SEASON_2018)
girls_varsity = Team.create(school: school, name: 'Girls Varsity', season: SEASON_2018)

# create a coach
coach = Coach.create(
  first_name: 'Kirk',
  last_name: 'Hyatt',
  phone_number: Faker::PhoneNumber.cell_phone,
  email: 'kirk.hyatt@example.com',
  team: boys_varsity
)
puts "Created coach #{coach.last_name}; #{coach.team.school.name} - #{coach.team.name}"

# create a second coach
puts "Creating coach Manny"
coach = Coach.create(
  first_name: 'Manny',
  last_name: 'Brown',
  phone_number: Faker::PhoneNumber.cell_phone,
  email: 'manny.brown@example.com',
  team: boys_jv
)
puts "Created coach #{coach.last_name}; #{coach.team.school.name} - #{coach.team.name}"

20.times do
  wrestler = Wrestler.create!(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name
  )
  puts "Created wrestler #{wrestler.id}: #{wrestler.last_name}"
  season = SeasonWrestler.create(wrestler_id: wrestler.id, team_id: boys_jv.id, academic_class: academic_class.sample, weight_class: weight_class.sample, season: SEASON_2018 )
end

20.times do
  wrestler = Wrestler.create(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name
  )
  puts "Created wrestler #{wrestler.id}: #{wrestler.last_name}"
  SeasonWrestler.create(wrestler_id: wrestler.id, team_id: boys_varsity.id, academic_class: academic_class.sample, weight_class: weight_class.sample, season: SEASON_2018 )
end

# create 3 more schools
3.times do
  # create school
  school = School.create(
    name: Faker::DrWho.character + ' HS',
    addr1: Faker::Address.street_address,
    city: Faker::Address.city,
    state_provence: 'WA',
    postal_code: Faker::Address.zip,
    mascot: Faker::DrWho.specie
  )
  puts "created school #{school}"

  # create a team for the school
  team = Team.create(school: school, name: 'Boys Varsity', season: SEASON_2018)
  puts "created team #{team}"

  # create a coach for the team
  coach = Coach.new(
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone_number: Faker::PhoneNumber.cell_phone,
    email: 'manny.brown@example.com',
    team: team
  )
  coach.email = "#{coach.first_name}.#{coach.last_name}@example.com"
  coach.save!
  puts "Created coach #{coach.last_name}; #{coach.team.school.name} - #{coach.team.name}"
  20.times do
    wrestler = Wrestler.create!(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name
    )
    puts "Created wrestler #{wrestler.id}: #{wrestler.last_name}"
    SeasonWrestler.create(wrestler_id: wrestler, team_id: team, academic_class: academic_class.sample, weight_class: weight_class.sample, season: SEASON_2018 )
  end
end

# create a meet
puts "Creating 2 meets"
meet = Meet.create(date: Date.today,   green_team: Team.find(1), red_team: Team.find(6))
meet = Meet.create(date: Date.today+7, green_team: Team.find(2), red_team: Team.find(4))
meet = Meet.create(date: Date.today+7, green_team: Team.find(4), red_team: Team.find(1))
meet = Meet.create(date: Date.today+7, green_team: Team.find(6), red_team: Team.find(2))
meet = Meet.create(date: Date.today+7, green_team: Team.find(5), red_team: Team.find(1))

match = Match.new

#   # create a team for the school for each season
#   t = Team.create(school: school, name: 'Boys Varsity')
#   t.seasons << season2018
#   t.seasons << season2017
#   t.seasons << season2016
#   t.seasons << season2015
#
#   # create a coach
#   2.times do
#     coach = Coach.new(
#       first_name: Faker::Name.first_name,
#       last_name: Faker::Name.last_name,
#       phone_number: Faker::PhoneNumber.cell_phone
#     )
#     coach.email = "#{coach.first_name}.#{coach.last_name}@example.com"
#     coach.save!
#     coach_season = CoachSeason.create!(coach: coach, season: season2018, team: t)
#     coach_season = CoachSeason.create!(coach: coach, season: season2017, team: t)
#     coach_season = CoachSeason.create!(coach: coach, season: season2016, team: t)
#     coach_season = CoachSeason.create!(coach: coach, season: season2015, team: t)
#   end
#
#   wc_160 = false
#   20.times do
#     wrestler = Wrestler.create(
#       first_name: Faker::Name.first_name,
#       last_name: Faker::Name.last_name
#     )
#     academic_class = ac.sample
#     if wc_160
#       weight_class = wc.sample
#     else
#       weight_class = 160
#       wc_160 = true
#     end
#
#     ws = WrestlerSeason.new(wrestler: wrestler, team: t, weight_class: weight_class)
#     case academic_class
#     when 12
#         ws[:season_id] = season2015.id
#         ws[:academic_class] = 9
#         ws.save!
#         ws[:season_id] = season2016.id
#         ws[:academic_class] = 10
#         ws.save!
#         ws[:season_id] = season2017.id
#         ws[:academic_class] = 11
#         ws.save!
#         ws[:season_id] = season2018.id
#         ws[:academic_class] = 12
#         ws.save!
#       when 11
#         ws[:season_id] = season2016.id
#         ws[:academic_class] = 9
#         ws.save!
#         ws[:season_id] = season2017.id
#         ws[:academic_class] = 10
#         ws.save!
#         ws[:season_id] = season2018.id
#         ws[:academic_class] = 11
#         ws.save!
#       when 10
#         ws[:season_id] = season2017.id
#         ws[:academic_class] = 9
#         ws.save!
#         ws[:season_id] = season2018.id
#         ws[:academic_class] = 10
#         ws.save!
#       else
#         ws[:season_id] = season2018.id
#         ws[:academic_class] = 9
#         ws.save!
#       end
#   end
# end
#
# # to create a match, you need to create a meet between 2 schools
# puts "Creating a Meet"
# school1 = School.find_by_name('Issaquah HS')
# school2 = School.last
# team1 = Team.find_by_school_id(school1)
# team2 = Team.last
#
# puts "Creating seasons for team_id = #{team1.id}"
# team1_season = TeamSeason.find_by_season_id_and_team_id(season2018.id, team1.id)
# puts "Creating seasons for team_id = #{team2.id}"
# team2_season = TeamSeason.find_by_season_id_and_team_id(season2018.id, team2.id)
#
# # create a meet
# meet = Meet.create(green_team: team1, red_team: team2, season: season2018, date: Date.today)
#
# # create a match
# # get a wrestler of the same weight_class from each team
# # roster = WrestlerSeason.where(season_id: season2018, team_id: team1)
# wrestler1 = Wrestler.find(WrestlerSeason.find_by_season_id_and_team_id_and_weight_class(season2018, team1, 160).wrestler_id)
# wrestler2 = Wrestler.find(WrestlerSeason.find_by_season_id_and_team_id_and_weight_class(season2018, team2, 160).wrestler_id)
#
# match = Match.create(meet: meet, red_wrestler: wrestler2,  green_wrestler: wrestler1)
#
# ms = MatchScore.new(wrestler: wrestler1, round: 1, match: match, description: 'Take Down', points: 2)
#
# MatchScore.create(wrestler: wrestler2, round: 1, match: match, description: 'Reversal', points: 2)
# MatchScore.create(wrestler: wrestler1, round: 1, match: match, description: 'Escape', points: 1)
# MatchScore.create(wrestler: wrestler1, round: 1, match: match, description: 'Take Down', points: 2)
# MatchScore.create(wrestler: wrestler1, round: 1, match: match, description: 'Near Fall2', points: 2)
# MatchScore.create(wrestler: wrestler2, round: 1, match: match, description: 'Escape', points: 1)
#
# MatchScore.create(wrestler: wrestler1, round: 2, match: match, description: 'Escape', points: 1)
# MatchScore.create(wrestler: wrestler2, round: 2, match: match, description: 'Take Down', points: 2)
# MatchScore.create(wrestler: wrestler2, round: 2, match: match, description: 'Near Fall2', points: 2)
#
# MatchScore.create(wrestler: wrestler1, round: 3, match: match, description: 'Near Fall 3', points: 3)
# MatchScore.create(wrestler: wrestler2, round: 3, match: match, description: 'Escape', points: 1)
# MatchScore.create(wrestler: wrestler1, round: 3, match: match, description: 'Take Down', points: 2)
# MatchScore.create(wrestler: wrestler1, round: 3, match: match, description: 'Near Fall 3', points: 3)
