class AddWrestlerToSeasonWrestler < ActiveRecord::Migration[5.1]
  def change
    add_reference :season_wrestlers, :wrestler, foreign_key: { to_table: :wrestlers }

  end
end
