class AddMatchCompleteToMatch < ActiveRecord::Migration[5.1]
  def change
    add_column :matches, :match_completed, :boolean, :default => false
    add_column :matches, :round, :integer, :default => 1 
  end
end
