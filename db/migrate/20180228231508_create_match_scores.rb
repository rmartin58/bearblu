class CreateMatchScores < ActiveRecord::Migration[5.1]
  def change
    create_table :matches_scores do |t|
      t.integer :wrestler_id
      t.integer :match_id
      t.integer :round, default: 1
      t.string :description
      t.integer :points
      t.timestamps
    end
  end
end
