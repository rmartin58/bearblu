class AddWrestersToMeet < ActiveRecord::Migration[5.1]
  def change
    add_reference :matches, :red_wrestler,   references: :wrestlers, index: true
    add_reference :matches, :green_wrestler, references: :wrestlers, index: true
  end
end
