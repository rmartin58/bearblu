class AddWeightClassToWrester < ActiveRecord::Migration[5.1]
  def change
    add_column :wrestlers, :weight_class, :integer
    add_column :wrestlers, :academic_class, :integer
  end
end
