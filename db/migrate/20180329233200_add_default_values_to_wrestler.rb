class AddDefaultValuesToWrestler < ActiveRecord::Migration[5.1]
  def change
    change_column :wrestlers, :win_decision, :integer, :default => 0
    change_column :wrestlers, :win_major_decision, :integer, :default => 0
    change_column :wrestlers, :win_forfeit, :integer, :default => 0
    change_column :wrestlers, :win_pin, :integer, :default => 0
    change_column :wrestlers, :loss_decision, :integer, :default => 0
    change_column :wrestlers, :loss_major_decision, :integer, :default => 0
    change_column :wrestlers, :loss_forfeit, :integer, :default => 0
    change_column :wrestlers, :loss_pin, :integer, :default => 0
    change_column :wrestlers, :take_downs, :integer, :default => 0
    change_column :wrestlers, :escapes, :integer, :default => 0
    change_column :wrestlers, :reversals, :integer, :default => 0
    change_column :wrestlers, :near_fall_2, :integer, :default => 0
    change_column :wrestlers, :near_fall_3, :integer, :default => 0
    change_column :wrestlers, :near_fall_4, :integer, :default => 0
    change_column :wrestlers, :penaties, :integer, :default => 0
    change_column :wrestlers, :cautions, :integer, :default => 0
    change_column :wrestlers, :disqualifications, :integer, :default => 0
  end
end
