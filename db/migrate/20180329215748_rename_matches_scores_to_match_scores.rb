class RenameMatchesScoresToMatchScores < ActiveRecord::Migration[5.1]
  def change
    rename_table :matches_scores, :match_scores
  end
end
