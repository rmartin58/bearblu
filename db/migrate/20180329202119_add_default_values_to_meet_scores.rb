class AddDefaultValuesToMeetScores < ActiveRecord::Migration[5.1]
  def change
    change_column :meets, :green_score, :integer, :default => 0
    change_column :meets, :red_score, :integer, :default => 0
  end
end
