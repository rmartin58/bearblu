class ExtendWrestler < ActiveRecord::Migration[5.1]
  def change
    drop_table :wrestler_seasons if ActiveRecord::Base.connection.table_exists? :wrestler_seasons

    add_column :wrestlers, :win_decision, :integer
    add_column :wrestlers, :win_major_decision, :integer
    add_column :wrestlers, :win_forfeit, :integer
    add_column :wrestlers, :win_pin, :integer
    add_column :wrestlers, :loss_decision, :integer
    add_column :wrestlers, :loss_major_decision, :integer
    add_column :wrestlers, :loss_forfeit, :integer
    add_column :wrestlers, :loss_pin, :integer
    add_column :wrestlers, :take_downs, :integer
    add_column :wrestlers, :escapes, :integer
    add_column :wrestlers, :reversals, :integer
    add_column :wrestlers, :near_fall_2, :integer
    add_column :wrestlers, :near_fall_3, :integer
    add_column :wrestlers, :near_fall_4, :integer
    add_column :wrestlers, :penaties, :integer
    add_column :wrestlers, :cautions, :integer
    add_column :wrestlers, :disqualifications, :integer
  end
end
