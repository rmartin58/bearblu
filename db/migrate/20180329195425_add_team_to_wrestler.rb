class AddTeamToWrestler < ActiveRecord::Migration[5.1]
  def change
    add_reference :wrestlers, :team, foreign_key: true
  end
end
