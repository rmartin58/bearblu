class CreateSeasonWrestlers < ActiveRecord::Migration[5.1]
  def change
    create_table :season_wrestlers do |t|
      t.string :season
      t.integer :wins_by_pin, :default => 0
      t.integer :wins_by_forfeit, :default => 0
      t.integer :wins_by_decision, :default => 0
      t.integer :wins_by_major_decision, :default => 0
      t.integer :losses_by_pin, :default => 0
      t.integer :losses_by_forfeit, :default => 0
      t.integer :losses_by_decision, :default => 0
      t.integer :losses_by_major_decision, :default => 0

      t.timestamps
    end
  end
end
