class AddTeamReferenceToSeasonWrestlers < ActiveRecord::Migration[5.1]
  def change
    remove_column :wrestlers, :team_id, :integer

    add_reference :season_wrestlers, :team, foreign_key: { to_table: :teams }
  end
end
