class AddSchoolReferenceToTeam < ActiveRecord::Migration[5.1]
  def change
    add_reference :teams, :school, index: true
  end
end
