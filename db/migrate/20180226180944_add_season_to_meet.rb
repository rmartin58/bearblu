class AddSeasonToMeet < ActiveRecord::Migration[5.1]
  def change
    add_reference :meets, :season, foreign_key: true
  end
end
