class CreateWrestlerSeasons < ActiveRecord::Migration[5.1]
  def change
    create_table :wrestler_seasons do |t|
      t.references :wrestler, foreign_key: true
      t.references :team, foreign_key: true
      t.references :season, foreign_key: true
      t.integer :win_decision
      t.integer :win_major_decision
      t.integer :win_forfeit
      t.integer :win_pin
      t.integer :loss_decision
      t.integer :loss_major_decision
      t.integer :loss_forfeit
      t.integer :loss_pin
      t.integer :weight_class
      t.integer :academic_class

      t.timestamps
    end
  end
end
