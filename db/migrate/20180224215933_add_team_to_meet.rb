class AddTeamToMeet < ActiveRecord::Migration[5.1]
  def change
    add_reference :meets, :red_team,   references: :teams, index: true
    add_reference :meets, :green_team, references: :teams, index: true
  end
end
