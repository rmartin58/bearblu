class AddWeightClassAndAcademicClassToSeasonWrestlers < ActiveRecord::Migration[5.1]
  def change
    add_column :season_wrestlers, :weight_class, :integer
    add_column :season_wrestlers, :academic_class, :integer

    remove_column :wrestlers, :weight_class, :integer
    remove_column :wrestlers, :academic_class, :integer
  end
end
