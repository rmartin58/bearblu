class DropObsoleteTableSeasonsTeams < ActiveRecord::Migration[5.1]
  def change
    drop_table :seasons_teams if ActiveRecord::Base.connection.table_exists? :seasons_teams

  end
end
