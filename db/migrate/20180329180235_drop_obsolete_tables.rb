class DropObsoleteTables < ActiveRecord::Migration[5.1]
  def change
    remove_column :meets, :season_id, :integer
    drop_table :wrestler_seasons if ActiveRecord::Base.connection.table_exists? :wrestler_seasons
    drop_table :coaches_seasons if ActiveRecord::Base.connection.table_exists? :coaches_seasons
    drop_table :seasons if ActiveRecord::Base.connection.table_exists? :seasons
    drop_table :scores if ActiveRecord::Base.connection.table_exists? :scores

  end
end
