class CreateScores < ActiveRecord::Migration[5.1]
  def change
    create_table :scores do |t|
      t.string :score_type
      t.integer :score_points

      t.timestamps
    end
  end
end
