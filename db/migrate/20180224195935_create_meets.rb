class CreateMeets < ActiveRecord::Migration[5.1]
  def change
    create_table :meets do |t|
      t.date :date
      t.integer :red_score
      t.integer :green_score

      t.timestamps
    end
  end
end
