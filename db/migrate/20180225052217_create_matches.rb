class CreateMatches < ActiveRecord::Migration[5.1]
  def change
    create_table :matches do |t|
      t.references :meet, foreign_key: true
      t.integer :green_score, :integer, default: 0, null: false
      t.integer :red_score, :integer, default: 0, null: false

      t.timestamps
    end
  end
end
