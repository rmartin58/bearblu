class CleanUpMatchesScores < ActiveRecord::Migration[5.1]
  def change
    add_foreign_key :matches_scores, :wrestlers
    add_foreign_key :matches_scores, :matches
  end
end
