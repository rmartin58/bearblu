class RemoveSeasonRecordFromWrestler < ActiveRecord::Migration[5.1]
  def change
    remove_column :wrestlers, :win_pin, :integer
    remove_column :wrestlers, :win_forfeit, :integer
    remove_column :wrestlers, :win_decision, :integer
    remove_column :wrestlers, :win_major_decision, :integer
    remove_column :wrestlers, :loss_pin, :integer
    remove_column :wrestlers, :loss_forfeit, :integer
    remove_column :wrestlers, :loss_decision, :integer
    remove_column :wrestlers, :loss_major_decision, :integer

  end
end
