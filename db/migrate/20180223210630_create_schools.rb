class CreateSchools < ActiveRecord::Migration[5.1]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :addr1
      t.string :addr2
      t.string :city
      t.string :state_provence
      t.string :postal_code
      t.string :mascot

      t.timestamps
    end
  end
end
