class AddDetailsToSeasonWrestler < ActiveRecord::Migration[5.1]
  def change
    remove_column :wrestlers, :take_downs, :integer
    remove_column :wrestlers, :escapes, :integer
    remove_column :wrestlers, :reversals, :integer
    remove_column :wrestlers, :near_fall_2, :integer
    remove_column :wrestlers, :near_fall_3, :integer
    remove_column :wrestlers, :near_fall_4, :integer
    remove_column :wrestlers, :penalties, :integer
    remove_column :wrestlers, :cautions, :integer
    remove_column :wrestlers, :disqualifications, :integer

    add_column :season_wrestlers, :take_downs, :integer, :default => 0
    add_column :season_wrestlers, :escapes, :integer, :default => 0
    add_column :season_wrestlers, :reversals, :integer, :default => 0
    add_column :season_wrestlers, :near_fall_2, :integer, :default => 0
    add_column :season_wrestlers, :near_fall_3, :integer, :default => 0
    add_column :season_wrestlers, :near_fall_4, :integer, :default => 0
    add_column :season_wrestlers, :penalties, :integer, :default => 0
    add_column :season_wrestlers, :cautions, :integer, :default => 0
    add_column :season_wrestlers, :disqualifications, :integer, :default => 0

  end
end
