class CreateCoachesSeasonsJoinTable < ActiveRecord::Migration[5.1]
  def change
    create_join_table :coaches, :seasons
  end
end
