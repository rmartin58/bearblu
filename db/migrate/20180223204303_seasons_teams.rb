class SeasonsTeams < ActiveRecord::Migration[5.1]
  def change
    create_join_table :teams, :seasons
  end
end
