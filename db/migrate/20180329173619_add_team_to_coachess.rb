class AddTeamToCoachess < ActiveRecord::Migration[5.1]
  def change
    add_reference :coaches, :team, foreign_key: true
  end
end
