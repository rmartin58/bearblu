class AddSeasonWinsLossesAndTiesToTeams < ActiveRecord::Migration[5.1]
  def change
    add_column :teams, :season, :string
    add_column :teams, :wins, :integer
    add_column :teams, :losses, :integer
    add_column :teams, :draws, :integer
    add_foreign_key :teams, :schools
  end
end
