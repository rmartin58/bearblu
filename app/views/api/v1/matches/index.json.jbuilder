json.array! @matches do |match|
  json.match match
  json.home_wrestler Wrestler.find(match.green_wrestler_id)
  json.visitor_wrestler Wrestler.find(match.red_wrestler_id)
  json.meet Meet.find(match.meet_id)
  end
