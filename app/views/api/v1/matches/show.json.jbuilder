json.status 'Success'
json.message "found a match with id #{@match.id}"
meet = Meet.find(@match.meet.id)
home_team = Team.find(meet.green_team_id)
home_school = School.find(home_team.school_id)
visitor_team = Team.find(meet.red_team_id)
visitor_school = School.find(visitor_team.school_id)
match_scores = MatchScore.where(match_id: @match.id).order(id: :asc)

home = {}
home[:home] = true
home[:wrestler] = Wrestler.find(@match.green_wrestler_id)
home[:team] = { school: home_school.name, team: home_team.name }
home[:score] = @match.green_score

visitor = {}
visitor[:home] = false
visitor[:wrestler] = Wrestler.find(@match.red_wrestler_id)
visitor[:team] = { school: visitor_school.name, team: visitor_team.name }
visitor[:score] = @match.red_score
wrestlers = [home, visitor]

json.season Season.find(meet.season_id).season
json.match_date meet.date
json.wrestlers wrestlers

# experimental
home_wrestler_name = "#{home[:wrestler].first_name} #{home[:wrestler].last_name}"
visitor_wrestler_name = "#{visitor[:wrestler].first_name} #{visitor[:wrestler].last_name}"

home_name = { name: home_wrestler_name }
visitor_name = { name: visitor_wrestler_name }
# annotated_scores = []
# new_score = {}
# match_scores.each do |score|
#   score.each_pair do |k, v|
#     new_score[k] = v
#   end
#   if new_score[:wrestler_id] == @match.green_wrestler_id
#     home_name
#   else
#     vistor_name
#   end
#   annotated_scores << new_score
# end

json.scores match_scores
