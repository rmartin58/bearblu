json.message "Found #{@teams.count} teams"
json.teams do
  json.array! @teams do |team|
    json.id team.id
    json.season team.season
    json.team team.name
    json.school School.find(team.school_id).name
  end
end
