json.array! @scores do |score|
  json.score score, :score_type, :score_points
end
