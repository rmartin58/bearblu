json.meets do
  json.array! @meets do |meet|
    home = Team.find(meet.green_team_id)
    visitor = Team.find(meet.red_team_id)

    json.id meet.id
    json.season home.season
    json.meet_date meet.date
    json.home_team do
      json.school School.find(home.school_id).name
      json.team home.name
      json.score meet.green_score
    end

    json.visiting_team do
      json.school School.find(visitor.school_id).name
      json.team visitor.name
      json.score meet.red_score
    end
  end
end
