json.status 'Success'
json.message 'WIP'
season_record = @wrestler_season[:wrestler][:season]
json.wrestler_season do
  json.season @wrestler_season[:season]
  json.wrestler @wrestler_season[:wrestler][:wrestler], :first_name, :last_name, :weight_class, :academic_class

  json.season_record do
    json.win_decision = season_record.win_decision
    json.win_major_decision = season_record.win_major_decision
    json.win_forfeit = season_record.win_forfeit
    json.wins
    json.win_pin = season_record.win_pin
    json.loss_decision = season_record.loss_decision
    json.loss_major_decision = season_record.loss_major_decision
    json.loss_forfeit = season_record.loss_forfeit
    json.loss_pin = season_record.loss_pin

    json.home_meets @wrestler_season[:wrestler][:home_meets].each do |meet|
      json.meet do
        json.date meet.date

        json.home_score meet.green_score
        json.home do
          school = School.find(meet.green_team_id)
          json.name school.name
          json.mascot school.mascot
        end

        json.vistor_score meet.red_score
        json.visitor do
          school = School.find(meet.red_team_id)
          json.name school.name
          json.mascot school.mascot
        end

        json.matches Match.meet(meet.id).each do |match|
          json.home_score match.green_score
          json.visitor_score match.red_score
        end
      end
    end

    json.visitor_meets @wrestler_season[:wrestler][:visitor_meets].each do |meet|
      json.date meet.date
      json.home meet.green_score
      json.visitor meet.red_score
      json.matches Match.meet(meet.id) do |match|
        json.match match, :green_score, :red_score
      end
    end
  end

  json.team do
    json.name @wrestler_season[:wrestler][:school].name
    json.mascot @wrestler_season[:wrestler][:school].mascot
    json.team @wrestler_season[:wrestler][:team].name
  end
end

# json.wrestler_season @wrestler_season
