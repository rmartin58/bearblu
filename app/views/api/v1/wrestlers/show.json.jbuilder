json.status 'Success'
json.message "found wrestler with id #{@wrestler.id}"
json.first_name @wrestler.first_name
json.last_name @wrestler.last_name
json.academic_class @wrestler.academic_class
json.weight_class @wrestler.weight_class
json.seasons @wrestler.seasons.each do |season|
  json.season season.season
  w_season = WrestlerSeason.find_by_wrestler_id_and_season_id(@wrestler.id, season.id)
  json.win_decision w_season.win_decision
  json.win_major_decision w_season.win_major_decision
  json.win_forfeit w_season.win_forfeit
  json.win_pin w_season.win_pin
  json.loss_decision w_season.loss_decision
  json.loss_major_decision w_season.loss_major_decision
  json.loss_forfeit w_season.loss_forfeit
  json.loss_pin w_season.loss_pin
end
