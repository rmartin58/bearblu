json.status 'ok'
json.message "Found #{@wrestlers.count} wrestlers"
json.wrestlers do
  json.array! @wrestlers do |wrestler|
    json.id wrestler.id
    json.wrestler wrestler.full_name
    seasons = wrestler.season_wrestlers
    json.seasons seasons.count
    json.seasons do
      json.array! seasons do |season|
        team = Team.find(season.team_id)
        school = School.find(team.school_id)
        json.season season.season
        json.school school.name
        json.team team.name
        json.academic_class season.academic_class
        json.weight_class season.weight_class
      end
    end
  end
end
