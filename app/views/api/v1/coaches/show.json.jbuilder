json.status 'Success'
json.message "found coach with id #{@coach.id}"
json.first_name @coach.first_name
json.last_name @coach.last_name
json.email @coach.email
json.phone_number @coach.phone_number
json.seasons CoachSeason.where(coach_id: @coach.id).each do |c_season|
  json.season Season.find(c_season.season_id).season
  json.school School.find(Team.find(c_season.team_id).school_id).name
  json.team Team.find(c_season.team_id).name
  json.wins TeamSeason.where(team_id: c_season.team_id, season_id: c_season.season_id)[0].wins
  json.losses TeamSeason.where(team_id: c_season.team_id, season_id: c_season.season_id)[0].losses
  json.draws TeamSeason.where(team_id: c_season.team_id, season_id: c_season.season_id)[0].draws
end
