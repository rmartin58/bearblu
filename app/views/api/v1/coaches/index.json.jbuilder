json.coaches do
  json.array! @coaches do |coach|
    team_id = coach.team_id
    team = Team.find(team_id)
    school = School.find(team.school_id)
    json.id coach.id
    json.name coach.full_name
    json.email coach.email
    json.phone_number coach.phone_number
    json.team do
      json.team_id team_id
      json.school school.name
      json.team team.name 
    end
  end
end
