json.status 'Success'
json.message "found coach with id #{@school.id}"
json.name @school.name
json.addr1 @school.addr1
json.addr2 @school.addr2
json.city @school.city
json.state_provence @school.state_provence
json.postal_code @school.postal_code
json.mascot @school.mascot
json.teams @school.teams do |team|
  json.team team.name
  json.seasons  TeamSeason.where(team_id: team.id) do |season|
    json.season season.season.season
    json.wins season.wins
    json.losses season.losses
    json.draws season.draws
  end
end
