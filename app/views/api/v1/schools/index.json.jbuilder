json.message "Found #{@schools.count} schools"
json.schools do
  json.array! @schools do |school|
    json.id school.id
    json.school school.name
    json.addr1 school.addr1
    json.addr2 school.addr2
    json.city school.city
    json.state school.state_provence
    json.postal_code school.postal_code
    json.mascot school.mascot
  end
end
