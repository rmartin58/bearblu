module Api
  module V1
    class WrestlersController < ApplicationController
      def index
        @wrestlers = Wrestler.all
        # render json: {status: 'Success', message: "Found #{@wrestlers.count} Wrestlers", data:@wrestlers }, status: :ok
      end

      def show
        @wrestler = Wrestler.find(params[:id])
        # render json: @wrestler
      end

      def create
        @wrestler = Wrestler.create(wrestler_params)
        @wrestler.save!
        render json: @wrestler, status: :ok
      end

      def update
        @wrestler = Wrestler.find(params[:id])
        @wrestler.update!(wrestler_params)
      end

      def wrestler_season
        @wrestler_season = {}
        @wrestler = Wrestler.find(params[:id])
        @wrestler_season[:season] = Season.find(params[:season_id]).season
        @wrestler_season[:wrestler] = @wrestler.my_season(params[:season_id])
        # render json: @wrestler_season
      end

      private

      def wrestler_params
        params.permit(:first_name, :last_name, :seasons)
      end
    end
  end
end
