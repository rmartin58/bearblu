module Api
  module V1
    class MatchScoresController < ApplicationController
      def index
        @match_scores = MatchScore.all
        render json: {status: 'Success', message: "Found #{@match_scores.count} Match Scores", data:@match_scores }, status: :ok
      end

      def create
        @match_score = MatchScore.new(match_score_params)
        @match_score.save!
        render json: @coach, status: :created
      end

      private

      def match_score_params
        params.permit(:wrestler_id, :match_id, :round, :points, :description)
      end

    end
  end
end
