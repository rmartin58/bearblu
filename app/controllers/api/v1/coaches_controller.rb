module Api
  module V1
    class CoachesController < ApplicationController
      def index
        @coaches = Coach.order('last_name')
        # render json: {status: 'Success', message: "Found #{@coaches.count} Coaches", data:@coaches }, status: :ok
      end

      def create
        @coach = Coach.new(coach_params)
        @coach.save!
        # render json: @coach, status: :created
      end

      def show
        @coach = Coach.find(params[:id])
        # render json: @coach, status: :created
      end

      def destroy
        @coach = Coach.where(id: params[:id]).first
        if @coach.destroy
          head(:ok)
        else
          head(:unprocessable_entity)
        end
      end

      private

      def coach_params
        params.permit(:first_name, :last_name, :email, :phone_number)
      end
    end # class CoachesController
  end # module V1
end # module API
