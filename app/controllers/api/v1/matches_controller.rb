class Api::V1::MatchesController < ApplicationController

  def index
    @matches = Match.all
    render json: {status: 'Success', message: "Found #{@matches.count} Matches", data:@matches }, status: :ok
  end

  def show
    @match = Match.find(params[:id])
    # render json: @match, status: :ok
  end


end
