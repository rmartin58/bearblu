module Api
  module V1
    class MeetsController < ApplicationController
      def index
        @meets = Meet.all
        # render json: {status: 'Success is here', message: "Found #{@meets.count} Meets", data:@meets }, status: :ok
      end

      def show
        @meet = Meet.find(params[:id])
        render json: {status: 'Success is here', message: "Found meet: #{@meet.id}", data:@meet }, status: :ok
      end


    end
  end
end
