module Api
  module V1
    class SessionsController < ApplicationController

      def create
        user = User.where(email: params[:email]).first

        if user&.valid_password?(params[:password])
          render json: {email: user[:email], authentication_token: user[:authentication_token]}, status: :ok

        else
          head(:unauthorized)
        end

      end

      def destroy
      end
    end
  end
end
