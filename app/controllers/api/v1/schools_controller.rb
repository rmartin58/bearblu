module Api
  module V1
    class SchoolsController < ApplicationController
      # acts_as_token_authentication_handler_for User, fallback: :none

      def index
          @schools = School.order('name')
          # render json: {status: 'Success', message: "Found #{@schools.count} Schools", data:@schools }, status: :ok
      end

      def show
        @school = School.find(params[:id])
        # render json: @school
      end

      def update
        @school.update(school_params)
        render json: @school
      end

      def create
        @school = School.create(school_params)
        render json: @school, status: :ok
      end

       private

      def school_params
        params.permit(:name, :addr1, :addr2, :city, :state_provence, :postal_code, :mascot)
      end
   end
  end
end
