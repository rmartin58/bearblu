module Api
  module V1
    class SeasonWrestlersController < ApplicationController
      # acts_as_token_authentication_handler_for User, fallback: :none

      def index
          @seasons = SeasonWrestler.all
          render json: {status: 'Success', message: "Found #{@seasons.count} seasons", data:@seasons }, status: :ok
      end
    end
  end
end
