module Api
  module V1
    class TeamsController < ApplicationController
      def index
        @teams = Team.order('name')
      end

      def show
        @team = Team.find(params[:id])
        render json: @team
      end

      def school
        @teams = Team.school(params[:id])
        render json: @teams
      end
    end
  end
end
