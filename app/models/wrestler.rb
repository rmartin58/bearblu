class Wrestler < ApplicationRecord
  has_many :season_wrestlers

  validates_presence_of :first_name, :last_name

  default_scope { order(last_name: :asc) }

  def full_name
    "#{first_name} #{last_name}"
    end

  def season_records
    SeasonWrestler.where(wrestler_id: self.id)
  end


  def update_point_accumulation(score)
    case score.description.downcase.parameterize.underscore
    when 'take_down'
      self.take_downs += 1
    when 'near_fall_2'
      self.near_fall_2 += 1
    when 'near_fall_3'
      self.near_fall_3 += 1
    when 'penalty'
      self.penalties += 1
    when 'escape'
      self.escapes += 1
    when 'reversal'
      self.reversals += 1
    when 'caution'
      self.cautions += 1
    when 'disqualification'
      self.disqualifications += 1
    else
      puts score.description.downcase.parameterize.underscore
    end
    self.save!
  end

  # def my_matches
  #   Match.wrestler(id)
  # end
  #
  # def team_season(s)
  #   season = {}
  #   season[:wrestler] = self
  #   season[:season] = WrestlerSeason.find_by_wrestler_id_and_season_id(self.id, s)
  #   season[:team] = Team.find(season[:season].team_id)
  #   season[:school] = School.find(season[:team].school_id)
  #   season
  # end
  #
  # def my_season(s)
  #   season = team_season(s)
  #
  #   home_matches = []
  #   season[:home_meets] = Meet.where(season_id: s, green_team_id: season[:team].id)
  #   season[:home_meets].each do |meet|
  #     home_matches = Match.where(green_wrestler_id: id, meet_id: meet.id)
  #   end
  #   season[:home_matches] = home_matches
  #
  #   visitor_matches = []
  #   season[:visitor_meets] = Meet.where(season_id: s, red_team_id: season[:team].id)
  #   season[:visitor_meets].each do |meet|
  #     visitor_matches = Match.where(red_wrestler_id: id, meet_id: meet.id)
  #   end
  #   season[:visitor_matches] = visitor_matches
  #
  #   season
  # end
end
