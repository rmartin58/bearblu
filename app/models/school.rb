class School < ApplicationRecord
  # acts_as_token_authenticatable
  has_many :teams
  validates_presence_of :name

  default_scope { order(name: :asc) }

end
