class Team < ApplicationRecord
  has_many :green_teams, foreign_key: :green_team_id, class_name: :Meet
  has_many :red_teams,   foreign_key: :red_team_id,   class_name: :Meet
  belongs_to :school

  validates_presence_of :name

  scope :school, lambda  { |school_id| where("(school_id = ?)", school_id )}

end
