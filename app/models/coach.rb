class Coach < ApplicationRecord
  belongs_to :team
  validates_presence_of :first_name, :last_name
  
  default_scope { order(last_name: :asc) }

  scope :from_team, ->(team) { where(team: team) }

  def full_name
    "#{first_name} #{last_name}"
  end
end
