class Meet < ApplicationRecord
  has_many :matches
  belongs_to :green_team, class_name: :Team
  belongs_to :red_team,   class_name: :Team

  validates_numericality_of :green_score, :red_score, greater_than_or_equal_to: 0, allow_nil: true

  scope :home_team, lambda { |t| where("green_team_id = ?", t) }
  scope :visiting_team, lambda { |t| where("red_team_id = ?", t) }
  scope :team,  lambda { |t| where("((green_team_id = ?) OR (red_team_id = ?))", t,t) }
  scope :teams, lambda { |t1,t2| where("(((green_team_id = ?) AND (red_team_id = ?)) OR ((green_team_id = ?) AND (red_team_id = ?)))",t1,t2,t2,t1)}
end
