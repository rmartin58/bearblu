class Match < ApplicationRecord
  belongs_to :meet
  belongs_to :green_wrestler, class_name: :Wrestler
  belongs_to :red_wrestler,   class_name: :Wrestler

  has_many :match_scores

  validates_numericality_of :green_score, :red_score, greater_than_or_equal_to: 0, allow_nil: true
  validates_numericality_of :round, greater_than_or_equal_to:  0, less_than: 4

  scope :home_wrestler, ->(w) { where('green_wrestler_id = ?', w) }
  scope :visiting_wrestler, ->(w) { where('red_wrestler_id = ?', w) }
  scope :wrestler,  ->(w) { where('((green_wrestler_id = ?) OR (red_wrestler_id = ?))', w, w) }
  scope :wrestlers, ->(w1, w2) { where('(((green_wrestler_id = ?) AND (red_wrestler_id = ?)) OR ((green_wrestler_id = ?) AND (red_wrestler_id = ?)))', w1, w2, w2, w1) }
  scope :meet, -> (m) { where('meet_id = ?', m)}

  # def wrestler_score(wrestler)
  #   wrestler_total_score = 0
  #   MatchScore.where(match: id, wrestler: wrestler).each do |score|
  #     wrestler_total_score += score.score.score_points
  #   end
  #   wrestler_total_score
  # end
  #
  # def score_sequence(wrestler)
  #   scores = []
  #   total_points = 0
  #   MatchScore.where(match: id, wrestler: wrestler).each do |score|
  #     scores << {
  #       round: score.round,
  #       score: score.score.score_type,
  #       points: score.score.score_points
  #     }
  #     total_points += score.score.score_points
  #   end
  #   { total_points: total_points, sequence: scores }
  # end
  #
  # def match_score
  #   {
  #     home_wrestler: wrestler_score(green_wrestler),
  #     visiting_wrestler: wrestler_score(red_wrestler)
  #   }
  # end
  #
  def update_match_score(score)
    # pp self
    wrestler = score.wrestler
    wrestler.update_point_accumulation(score)
    if wrestler == green_wrestler
      self.green_score += score.points
    elsif wrestler == red_wrestler
      self.red_score += score.points
    else
      logger.error 'error: need to throw an exception; this wrestler is not in this match'
    end
    save!
  end
end
