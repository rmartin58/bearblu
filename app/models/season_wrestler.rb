class SeasonWrestler < ApplicationRecord
  belongs_to :wrestler
  belongs_to :team

  scope :roster, lambda  { |season, team| where("(season = ?) and (team_id = ?)", season, team )}
end
