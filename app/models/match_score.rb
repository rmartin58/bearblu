class MatchScore < ApplicationRecord
  belongs_to :match
  belongs_to :wrestler

  validates_presence_of :match_id, :wrestler_id, :round, :points, :description

  after_create :update_match_score

  def update_match_score
    Match.find(self.match_id).update_match_score(self)
  end
end
