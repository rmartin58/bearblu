class Score < ApplicationRecord

  validates_presence_of :score_type
  validates_numericality_of :score_points, greater_than: 0, less_than: 5
  # validates_numericality_of :score_points,  { greater_than: o, less_than: 6 }

  default_scope { order(score_points: :desc) }

end
