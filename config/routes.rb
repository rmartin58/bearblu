Rails.application.routes.draw do
  devise_for :users
  namespace 'api' do
    namespace 'v1', defaults: { format: :json } do
      get "teams/school" # => "teams#school", as: :school_teams
      resources :teams
      resources :wrestlers
      resources :season_wrestlers
      resources :coaches
      resources :schools
      resources :scores
      resources :matches
      resources :match_scores
      resources :meets
      resources :sessions, only: [:create, :destroy]
    end
  end
end
