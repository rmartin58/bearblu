FactoryBot.define do
  factory :match do
    meet
    association :green_wrestler, factory: :wrestler
    association :red_wrestler,   factory: :wrestler
  end
end
