FactoryBot.define do
  factory :meet do
    date Date.today
    association :green_team, factory: :team, strategy: :create
    association :red_team, factory: :team, strategy: :create
  end
end
