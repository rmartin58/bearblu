FactoryBot.define do
  factory :school do
    name "A School"
    addr1 "Addr1"
    addr2 "Addr2"
    city "Cty"
    state_provence "State"
    postal_code "postal_code"
    mascot "Mascot"
  end
end
