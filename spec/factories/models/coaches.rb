FactoryBot.define do
  factory :coach do
    first_name "Joe"
    last_name "Cool"
    team
  end
end
