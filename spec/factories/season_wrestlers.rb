FactoryBot.define do
  factory :season_wrestler do
    season "MyString"
    wins_by_pin 1
    wins_by_forfeit 1
    wins_by_decision 1
    wins_by_major_decision "MyString"
    losses_by_pin 1
    losses_by_forfeit "MyString"
    losses_by_decision 1
    losses_by_major_decision "MyString"
  end
end
