require 'rails_helper'

RSpec.describe School, type: :model do
  before(:all) do
    School.create(name: 'Hufflepuff')
    School.create(name: 'Gryffindor')
    School.create(name: 'Slytherin')
    School.create(name: 'Ravenclaw')
  end

  after(:all) do
    # Team.destroy_all
    School.destroy_all
  end

  before(:each) do
  end

  it 'should be alphabetized' do
    expect(School.first.name).to eq 'Gryffindor'
    expect(School.last.name).to eq 'Slytherin'
  end

  it { should validate_presence_of(:name) }
  it { should have_many(:teams) }

  it 'should exist' do
    count = School.count
    expect(count).to eq(4)
    school = School.create(name: 'Hogwarts')
    expect(School.count).to eq(count + 1)
    school.delete
    expect(School.count).to eq(count)
  end
end
