require 'rails_helper'

RSpec.describe Meet, type: :model do

  describe 'associations' do
    it { should belong_to(:green_team) }
    it { should belong_to(:red_team) }
    it { should have_many(:matches) }
  end

  describe 'validations' do
    it { should validate_numericality_of(:green_score).is_greater_than_or_equal_to(0) }
    it { should validate_numericality_of(:red_score).is_greater_than_or_equal_to(0) }
  end

  describe 'scopes' do
    it 'has 2 teams' do
      meet = FactoryBot.create(:meet)
      expect(Meet.home_team(meet.green_team).count).to eq(1)
      expect(Meet.visiting_team(meet.red_team).count).to eq(1)
    end
  end
end
