require 'rails_helper'

RSpec.describe Wrestler, type: :model do

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end

  describe 'associations' do
    it { should have_many(:season_wrestlers)}
  end

  describe 'methods' do
    it 'should respnd with the formatted full name' do
      @wrestler = FactoryBot.create(:wrestler)
      expect(@wrestler.full_name).to eq("#{@wrestler.first_name} #{@wrestler.last_name}")
    end

    it 'should respond with season records' do
      @wrestler = FactoryBot.create(:wrestler)
      expect(@wrestler.season_records).to eq([])
    end
  end
end
