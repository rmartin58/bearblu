require 'rails_helper'

RSpec.describe Team, type: :model do

  it { should belong_to(:school) }
  it { should have_many(:green_teams) }
  it { should have_many(:red_teams) }

  it { should validate_presence_of(:name)}

  it "should exist" do
    @school = FactoryBot.create(:school)
    team = Team.create(school: @school, name: Faker::Dune.character + " HS")
    expect(Team.count).to eq(1)
    team = Team.create(school: @school, name: Faker::Dune.character + " HS")
    expect(Team.count).to eq(2)
    team.delete
    expect(Team.count).to eq(1)
  end

  describe 'team_school' do
    it 'should return a hash with the school id, name, and mascot' do
      school = FactoryBot.create(:school)
      team = Team.create(school: school, name: Faker::Dune.character + " HS")
      expect(team.school[:id]).to eq(school.id)
      expect(team.school[:name]).to eq(school.name)
      expect(team.school[:mascot]).to eq(school.mascot)
    end
  end
end
