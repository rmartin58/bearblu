require 'rails_helper'

RSpec.describe Match, type: :model do
  describe 'associations' do
    it { should belong_to(:meet) }
    it { should belong_to(:green_wrestler) }
    it { should belong_to(:red_wrestler) }

    it { should have_db_column(:red_score) }
    it { should have_db_column(:green_score) }
  end

  describe 'validations' do
    it { should validate_numericality_of(:green_score).is_greater_than_or_equal_to(0) }
    it { should validate_numericality_of(:red_score).is_greater_than_or_equal_to(0) }
  end


  # describe 'respond to methods' do
  #   it { should respond_to(:wrestler_score) }
  #   it { should respond_to(:score_sequence) }
  #   it { should respond_to(:match_score) }
  #   it { should respond_to(:update_match_score) }
  # end
end
