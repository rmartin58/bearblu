require 'rails_helper'

RSpec.describe Coach, type: :model do

  describe 'validations' do
    it { should validate_presence_of(:first_name) }
    it { should validate_presence_of(:last_name) }
  end

  describe 'associations' do
    it { should belong_to(:team) }
  end

  describe 'methods' do
    context 'it responds_to .full_name' do
      it { should respond_to(:full_name) }
    end

    context "it responds with 'first_name last_name' formatted string" do
      it 'should respond with full name' do
        @coach = FactoryBot.create(:coach)
        expect(@coach.full_name).to eq "#{@coach.first_name} #{@coach.last_name}"
      end
    end
  end

  describe 'default order' do
    it 'should be in alphabetical order on last_name' do
      FactoryBot.create(:coach, first_name: 'Harry', last_name: 'Houdini')
      FactoryBot.create(:coach, first_name: 'Albert', last_name: 'Einstien')
      FactoryBot.create(:coach, first_name: 'Jim', last_name: 'Annders')
      coach = Coach.first
      expect(coach.last_name).to eq('Annders')
      coach = Coach.last
      expect(coach.last_name).to eq('Houdini')
    end
  end

  describe 'creation and deletion' do
    it 'should exist' do
      @coach = FactoryBot.create(:coach)
      expect(Coach.count).to eq(1)
      @coach = FactoryBot.create(:coach)
      expect(Coach.count).to eq(2)
      @coach.delete
      expect(Coach.count).to eq(1)
    end
  end

  describe 'methods' do
    it 'should respnd with a string containing the full name' do
      @coach = FactoryBot.create(:coach)
      expect(@coach.full_name).to eq("#{@coach.first_name} #{@coach.last_name}")
    end
  end
end
