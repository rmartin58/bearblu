require 'rails_helper'

RSpec.describe MatchScore, type: :model do
  describe 'associations' do
    it { should belong_to :match }
    it { should belong_to :wrestler }
  end

  describe 'validations' do
    it { should validate_presence_of :round }
    it { should validate_presence_of :wrestler_id }
    it { should validate_presence_of :points }
    it { should validate_presence_of :description }
    it { should validate_presence_of :match_id }
  end

  describe 'methods' do
    it { should respond_to(:update_match_score) }
  end

  # describe 'match score updates match and wrestler' do
    # it 'should have a meet' do
    #   match = FactoryBot.create(:match)
    #   meet = match.meet
    #   expect(meet.valid?).to be true
    #   home_wrestler = match.green_wrestler
    #   expect(home_wrestler.valid?).to be true
    #   visitor_wrestler = match.red_wrestler
    #   expect(visitor_wrestler.valid?).to be true
    #
    #   match_score = MatchScore.create(
    #     match: match,
    #     round: 1,
    #     wrestler: home_wrestler,
    #     description: 'take_down',
    #     points: 2
    #   )
    #
    #   expect(MatchScore.count).to eq(1)
    #   match.reload
    #   expect(match.green_score).to eq(2)
    #   expect(home_wrestler.take_downs).to eq(1)
    #
    #   match_score = MatchScore.create(
    #     match: match,
    #     round: 1,
    #     wrestler: home_wrestler,
    #     description: 'near_fall_3',
    #     points: 3
    #   )
    #   match.reload
    #   expect(match.green_score).to eq(5)
    #   expect(home_wrestler.near_fall_3).to eq(1)
    #
    #   match_score = MatchScore.create(
    #     match: match,
    #     round: 1,
    #     wrestler: visitor_wrestler,
    #     description: 'reversal',
    #     points: 2
    #   )
    #   match.reload
    #   expect(match.green_score).to eq(5)
    #   expect(match.red_score).to eq(2)
    #   expect(visitor_wrestler.reversals).to eq(1)
    # end
  # end
end
